# Almost Linux CPU clock monitor program for Linux desktop.
* This source code was designed for RK3399 ( or similar ) debian desktop, but enhanced to works almost of Embedded Linux even desktop versions of x86 32/64.
* Displays CPU and GPU temperature in every second.
* Automatically ignore if GPU sensor not detected.
* Displays CPU clocks for each cores in maximum 64.

## Graph explanation
* This program not monitors CPU load.
* Graphs meaning temperature of CPU(purple) and GPU(green).
* Bar graphs meaning CPU clocks and it appears to move together by CPU architecture, or grouping policy.

## Latest update

### version 0.4.3.47
* fixed a bug cannot sense which is CPU for Raspberry Pi series.

## Previous updates

### version 0.4.3.45
* automatically sens which is CPU and GPU.
* non-sensed GPU may displays 0.0 'C.

### version 0.4.3.44
* now applicaiton icon shows on window title !

### version 0.4.2.43
* fl_imgtk v0.3.31.1, some GUI updates in drawing graph.

## Build requirements :
* FLTK-1.3.5-2-ts
* fl_imgtk v0.3.31.1 or higher version required.

## How to build ?
* Need to compile and install 'FLTK-1.3.5-2-ts' to system.
* Need to compile and place same directory level for 'fl_imgtk'.
* Just type 'make'
* Then 'sudo make install' for install to desktop application.

### x86.64
* Some platform requires -std=c++11, remove comment at Makefile for assign -std=c++11.

## Works for another ?
* Works well for Raspberry Pi3B+, even x86.
* Or other embedded Linux too if it uses X11.

## Other Copyrights
* Erco, Bill Spitzak and others of FLTK.
* Raphael Kim ( rageworx - at - gmail )

## Lincese
* FLTK-License, and LGPL.
