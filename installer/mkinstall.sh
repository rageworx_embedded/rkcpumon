#!/bin/bash

DATETIME=`date +%F-%H-%M-%S`
APPNAME=rkcpumon

TARSRC="/usr/local/bin/${APPNAME}"
TARSRC+=" /usr/share/applications/${APPNAME}.desktop"
TARSRC+=" /usr/local/share/icons/hicolor/256x256/apps/${APPNAME}.png"

TARFN="${APPNAME}-${DATETIME}-package.tar.gz"

tar -cvzf ${TARFN} ${TARSRC}
