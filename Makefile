# Makefile for RaphKay's Linux cpu monitoring information.
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
GCC = gcc
GPP = g++
AR  = ar

# TARGET settings
TARGET_PKG = rkcpumon
TARGET_DIR = ./bin
TARGET_OBJ = ./obj/Release

# Installation paths
INST_PATH  = /usr/local/bin
DESK_PATH  = /usr/share/applications
IRES_PATH  = /usr/local/share/icons/hicolor/256x256/apps

# Compiler optiops
COPTS += -std=c++11
COPTS += -ffast-math -fexceptions -fopenmp -O3 -s

# FLTK configs
FCG = fltk-config --use-images
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# FLTK image toolkit
FLIMGTK = ../fl_imgtk/lib

# Sources
SRC_PATH = src
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)

# CC FLAG
CFLAGS  = -I$(SRC_PATH)
CFLAGS += $(FLTKCFG_CXX)
CFLAGS += -I$(FLIMGTK)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# LINK FLAG
#LFLAGS  = -static
LFLAGS += -L$(FLIMGTK)
LFLAGS += -lfl_imgtk
LFLAGS += -lpthread
LFLAGS += $(FLTKCFG_LFG)

.PHONY: prepare clean

all: prepare continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

install: $(TARGET_DIR)/$(TARGET_PKG)
	@mkdir -p $(IRES_PATH)
	@cp -rf $< $(INST_PATH)
	@cp -rf res/rkcpumon.png $(IRES_PATH)
	@./mkinst.sh $(INST_PATH) $(DESK_PATH)

uninstall: $(INST_PATH)/$(TARGET_PKG)
	@rm -rf $(INST_PATH)/$(TARGET_PKG)
	@rm -rf $(IRES_PATH)/rkcpumon.png
	@rm -rf $(DESK_PATH)/rkcpumon.desktop

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS)
	@echo "Generating $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."

