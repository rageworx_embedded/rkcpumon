// Programmed by Raphael Kim.
//
#include <unistd.h>
#include <signal.h>
#include <dirent.h>

#ifdef __APPLE__
#include <sys/sysctl.h>
#endif /// of __APPLE__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include <string>
#include <vector>
#include <array>
#include <algorithm>
#include <fstream>

#include <pthread.h>

#include <FL/x.H>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_PNG_Image.H>
#include "Fl_MulticolorBarGraph.H"

#include "fl_imgtk.h"
#include "cputk.h"
#include "version.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////

// checks fl_imgtk version here.
#if ( FL_IMGTK_VERSION < 303821 )
    #error "Error, Need FL_IMGTK version 0.3.38.21 or higher"
#endif

/////////////////////////////////////////////////////////////////////////

#define DEF_APP_CLS          "rkcpumon"
#define DEF_APP_NAME         "RaphKay's CPU Monitor"

#ifndef __APPLE__
#define DEF_CPUINFO          "/proc/cpuinfo"
#define DEF_CPU_PRESENT      "/sys/devices/system/cpu/present"
#define DEV_VIRTUAL_TEMP_DIR "/sys/devices/virtual/thermal"
#define DEF_VIRTUAL_TEMP_MAP "/sys/devices/virtual/thermal/thermal_zone%u/temp"
#define DEF_CPU_MAX_CLK_MAP  "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_max_freq"
#define DEF_CPU_MIN_CLK_MAP  "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_min_freq"
#define DEF_CPU_CUR_CLK_MAP  "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_cur_freq"
#else
#define DEF_CPUINFO          "sysctl -a"
#define DEF_CPU_PRESENT      "sysctl hw | grep physicalcpu_max"
#define DEV_VIRTUAL_TEMP_DIR "/sys/devices/virtual/thermal"
#define DEF_VIRTUAL_TEMP_MAP "/sys/devices/virtual/thermal/thermal_zone%d/temp"
#define DEF_CPU_MAX_CLK_MAP  "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_max_freq"
#define DEF_CPU_MIN_CLK_MAP  "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_min_freq"
#define DEF_CPU_CUR_CLK_MAP  "/sys/devices/system/cpu/cpu%u/cpufreq/scaling_cur_freq"
#endif /// of __APPLE__

#define DEF_ERROR_TEMP      (-99.f)
#define DEF_SHIFT_PIXELS    (30)
#define DEF_SHIFT_MIGR      (-3)
#define DEF_DRAW_BG_COL_0   (0x330000FF)
#define DEF_DRAW_BG_COL_1   (0x330000FF)
#define DEF_DRAW_BG_COL_2   (0x150000FF)
#define DEF_DRAW_BG_COL_3   (0x000015FF)
#define DEF_DRAW_BG_COL_4   (0x222230FF)
#define DEF_DRAW_BG_STRIPE  (0x552222FF)
#define DEF_DRAW_LINE_THCK  (3.f)
#define DEF_DRAW_LINE_COL_G (0x33FF3397)
#define DEF_DRAW_LINE_COL_C (0xFF33FF97)

// to handle UTF-8 charactors - 
#define INT_STR_PAIR(i, s) count_bytes(s, i), s

/////////////////////////////////////////////////////////////////////////

static string               strAppTitle;
static string               cpu_model;
static string               cpu_hardware;
static string               cpu_revision;
static string               cpu_disp_name;
static string               cpu_disp_features;

static Fl_Double_Window*    winMain = NULL;
static Fl_Box*              boxGraphs = NULL;
static Fl_Output*           outCPUname = NULL;
static Fl_Output*           outCPUfeatures = NULL;
static vector< Fl_Output* > outCPUfreqMinMax;
static vector< Fl_Output* > outCPUfreqCur;
static vector< MulticolorBarGraph* >
                            mbgCPUfreq;
static vector< Fl_Box* >    boxMHzdp;
static Fl_Output*           outCPUtemper = NULL;
static Fl_Output*           outGPUtemper = NULL;
static Fl_RGB_Image*        imgGraphs = NULL;
static Fl_Box*              boxCopyright = NULL;
static Fl_RGB_Image*        winIcon = NULL;

static vector< float >      cpu_freq_min;
static vector< float >      cpu_freq_max;
static vector< float >      cpu_freq_cur;
static unsigned             cpu_cnt_min;
static unsigned             cpu_cnt_max;
static float                cpu_freq_maxall = 0.f;
static float                cpu_temper = 0.f;
static float                gpu_temper = 0.f;
static int                  cpu_tmp_idx = -1;
static int                  gpu_tmp_idx = -1;

const char*                 ws = " \t\n\r\f\v";
static bool                 threadlived = true;
static pthread_t            pt = 0;

/////////////////////////////////////////////////////////////////////////

size_t arraylen   = 0;
size_t arraydiv   = 4;
int    temp_thres = 100;
int    temp_max   = 150;

vector<int> arrayGPUt;
vector<int> arrayCPUt;

/////////////////////////////////////////////////////////////////////////

static int count_bytes(char const *utf8_string, int length)
{
    char const *s = utf8_string;
    for (;;)
    {
        int ch = *(unsigned char *)s++;
        if ((ch & 0xc0) == 0xc0) // first byte of a multi-byte UTF-8
            while (((ch = *(unsigned char*)s) & 0xc0) == 0x80)
                ++s;
        if (ch == 0)
            break;
        if (--length <= 0)
            break;
    }
    return s - utf8_string;
}

bool readf( const char* fn, char* data, size_t datalen )
{
    bool retb = false;

    FILE* fp = fopen( fn, "r" );
    if ( fp != NULL )
    {
        if ( fgets( data, datalen, fp ) == NULL )
        {
            fprintf( stderr, "Cannot read data from %s\n", fn );
        }
        else
        {
            retb =true;
        }

        fclose( fp );

        return retb;
    }

    return retb;
}

bool readfln( const char* fn, vector< string > &out )
{
    ifstream ifs( fn );
    
    if ( ifs.is_open() == false )
        return false;

    string tmp;

    while( getline( ifs, tmp ) )
    {
        out.push_back( tmp );
    }

    return true;
}

void sensetmpdevs()
{
	int tz_i = -1;

    // find which is GPU and CPU in virtual thermal zone.
    DIR* pDir = opendir( DEV_VIRTUAL_TEMP_DIR );
    if ( pDir != NULL )
    {
        struct dirent* pDE = ::readdir( pDir );

        while( pDE != NULL )
        {
            string tmpnm =  pDE->d_name;

            if ( tmpnm.find( "thermal_zone" ) != string::npos )
            {
				if ( tz_i < 0 )
					tz_i = 1;
				else
					tz_i++;

                char tmpmap[256] = {0};
                char devtp[64] = {0};
                int  devid = 0;

                string stpx = tmpnm.substr( 12 );
                devid = atoi( stpx.c_str() );

                snprintf( tmpmap, 256,
                          "%s/%s/type",
                          DEV_VIRTUAL_TEMP_DIR,
                          pDE->d_name );

                if( readf( tmpmap, devtp, 64 ) == true )
                {
                    string strid = devtp;

                    // find CPU
                    if ( strid.find( "soc-thermal" ) != string::npos )
                    {
                        if ( cpu_tmp_idx < 0 )
                            cpu_tmp_idx = devid;
                    }
                    else
                    if ( strid.find( "_pkg_temp" ) != string::npos )
                    {
                        if ( cpu_tmp_idx < 0 )
                            cpu_tmp_idx = devid;
                    }
                    else
                    if ( strid.find( "cpu-" ) != string::npos )
                    {
                        if ( cpu_tmp_idx < 0 )
                            cpu_tmp_idx = devid;
                    }

                    // find GPU 
                    if ( strid.find( "gpu-" ) != string::npos )
                    {
                        if ( gpu_tmp_idx < 0 )
                            gpu_tmp_idx = devid;
                    }
                    else
                    if ( strid.find( "vpu-" ) != string::npos )
                    {
                        if ( gpu_tmp_idx < 0 )
                            gpu_tmp_idx = devid;
                    }
                }
            }
            pDE = readdir( pDir );
        }

        closedir( pDir );
    }

	if ( ( tz_i >= 0 ) && ( cpu_tmp_idx < 0 ) )
		cpu_tmp_idx = 0;
}

// detype 
//   0 : CPU
//   1 : GPU
float read_temperature( unsigned dtype )
{
    int realidx = -1;

    switch( dtype )
    {
        case 0: /// CPU
            if ( cpu_tmp_idx < 0 )
                return DEF_ERROR_TEMP;
            realidx = cpu_tmp_idx;
            break;
        
        case 1: /// GPU
            if ( gpu_tmp_idx < 0 )
                return DEF_ERROR_TEMP;
            realidx = gpu_tmp_idx;
            break;

        default:
            return 0.0f;
    }

    char dev_map[256] = {0};
    snprintf( dev_map, 256, DEF_VIRTUAL_TEMP_MAP, realidx );

    char data[32] = {0};

    // bug fix
    // temperatue is not always 5 length.
    // high [2] and low [4] may belower than 10C' means [1]
    // chekc all length and, divide it by length - 4.
    if ( readf( dev_map, data, 32 ) == true )
    {
        size_t datalen = strlen(data);

        if ( datalen >= 4 )
        {
            char tmp_h[4] = {0};
            char tmp_l[4] = {0};

            size_t tmpsz_h = datalen - 4;
            memcpy( tmp_h, data, tmpsz_h );
            memcpy( tmp_l, &data[tmpsz_h], 3 );

            char conv_s[32] = {0};

            snprintf( conv_s, 32, "%s.%s", tmp_h, tmp_l );

            return atof( conv_s );
        }
    }

    return DEF_ERROR_TEMP;
}

vector< string> split_string(const string& str, const string& delimiter)
{
    vector< string > strings;

    string::size_type pos = 0;
    string::size_type prev = 0;

    while ( (pos = str.find(delimiter, prev) ) != string::npos )
    {
        strings.push_back(str.substr(prev, pos - prev));
        prev = pos + 1;
    }

    strings.push_back(str.substr(prev));

    return strings;
}

inline string& rtrim( string& s, const char* t = " \t\n\r\f\v" )
{
    s.erase(s.find_last_not_of(t) + 1);
    return s;
}

inline string& ltrim( string& s, const char* t = " \t\n\r\f\v" )
{
    s.erase(0, s.find_first_not_of(t));
    return s;
}

inline string& trim( string& s, const char* t = " \t\n\r\f\v" )
{
    return ltrim(rtrim(s, t), t);
}

#ifdef __APPLE__
void read_cpu_info_proc()
{
    char   tmpbuff[256] = {0};
    size_t tmpbuffsz = 256;

    int reti = sysctlbyname( "machdep.cpu.brand_string", 
                             &tmpbuff, &tmpbuffsz, NULL, 0 );
    if ( reti >= 0 )
    {
        cpu_model = tmpbuff;
    }

    tmpbuffsz = 256;
    reti = sysctlbyname( "hw.model", 
                         &tmpbuff, &tmpbuffsz, NULL, 0 );
    if ( reti >= 0 )
    {
        cpu_model += "(";
        cpu_model += tmpbuff;
        cpu_model += ")";
    }
 
    tmpbuffsz = 256;
    reti = sysctlbyname( "hw.optional.*", 
                         &tmpbuff, &tmpbuffsz, NULL, 0 );
    if ( reti >= 0 )
    {
        cpu_disp_features = tmpbuff;
    }



    cpu_disp_name  = cpu_model;

    if ( cpu_hardware.size() > 0 )
    {
        cpu_disp_name += " | ";
        cpu_disp_name += cpu_hardware;
    }

    if ( cpu_revision.size() > 0 )
    {
        cpu_disp_name += ", ";
        cpu_disp_name += cpu_revision;
    }

}
#else
void read_cpu_info_proc()
{
#ifdef __APPLE__
    char dev_map[256] = {0};
    
    sprintf( dev_map, "%s > /private/tmp/cpuinfo", DEF_CPUINFO );
    system( dev_map );
    sprintf( dev_map, "/private/tmp/cpuinfo" );
#endif /// of __APPLE__

    vector< string > infolns;

#ifndef __APPLE__
    if ( readfln( DEF_CPUINFO, infolns ) == true )
#else
    if ( readfln( dev_map, infolns ) == true )
#endif
    {
#ifdef __APPLE__
        unlink( dev_map );
#endif
        // parse it !
        for( size_t cnt=0; cnt<infolns.size(); cnt++ )
        {
            vector< string > spls = split_string( infolns[cnt], ":" );

            if ( spls.size() >= 2 )
            {
#ifndef __APPLE__
                if ( spls[0].find( "model name" ) != string::npos )
#else
                if ( spls[0].find( ".brand_string" ) != string::npos )
#endif
                {
                    if ( cpu_model.size() == 0 )
                    {
                        cpu_model = trim( spls[1] );
                    }
                    else
                    if ( cpu_model != trim( spls[1] ) )
                    {
                        cpu_model += ", ";
                        cpu_model += trim( spls[1] );
                    }
                }
                else
#ifndef __APPLE__
                if ( spls[0].find( "Features" ) != string::npos )
#else
                if ( spls[0].find( "hw.cpu" ) != string::npos )
#endif
                {
#ifndef __APPLE__
                    if ( cpu_disp_features.size() == 0 )
                    {
                        cpu_disp_features = trim( spls[1] );
                    }
#else
                    long lv = atol( spls[1].c_str() );
                    if ( lv > 0 )
                    {
                        char tmps[32] = {0};
                        snprintf( tmps, 32, "%s(%lX) ",
                                  spls[0].substr(6).c_str(),
                                  lv );

                        cpu_hardware += tmps;
                    }
#endif
                }
                else
                // x86 architectures
#ifndef __APPLE__
                if ( spls[0].find( "flags" ) != string::npos )
#else
                if ( spls[0].find( "hw.optional." ) != string::npos )
#endif
                {
#ifndef __APPLE__
                    if ( cpu_disp_features.size() == 0 )
                    {
                        cpu_disp_features = trim( spls[1] );
                    }
#else
                    long lv = atol( spls[1].c_str() );
                    if ( lv > 0 )
                    {
                        char tmps[32] = {0};
                        snprintf( tmps, 32, "%s(%lX) ",
                                  spls[0].substr(12).c_str(),
                                  lv );

                        cpu_disp_features += tmps;
                    }
#endif
                }
                else
                if ( spls[0].find( "Hardware" ) != string::npos )
                {
                    if ( cpu_hardware.size() == 0 )
                    {
                        cpu_hardware = trim( spls[1] );
                    }
                }
                else
                if ( spls[0].find( "Revision" ) != string::npos )
                {
                    if ( cpu_revision.size() == 0 )
                    {
                        cpu_revision = trim( spls[1] );
                    }
                }
            }
        }
    }
    else
    {
        cpu_model = "ARM";
        cpu_hardware = "unknown";
        cpu_revision = "N/A";

#ifdef CPUNAME
        cpu_model = CPUNAME;
#endif
#ifdef CPUSUBNAME
        cpu_hardware = CPUSUBNAME;
#endif
    }

    // checks if it failed to find cpu model.
    if ( cpu_model.size() < 2 )
    {
        vector< string > cpun;
        unsigned cn = serach_cpu_dirs( cpun );
        if ( cn > 0 )
        {
            size_t cutlen = 0;
            vector< string > cpls = split_string( cpun[0], "_" );
            if ( cpls.size() > 0 )
            {
                cpu_model = cpls[0];
                cutlen = cpu_model.size() + 1;
            }

            cpu_hardware = cpun[0].substr( cutlen );
            if ( cn > 1 )
            {
                for( unsigned x=1; x<cn; x++ )
                {
                    cpu_hardware += ", ";
                    cpu_hardware += cpun[x].substr( cutlen );
                }
            }
        }
    }

    cpu_disp_name  = cpu_model;

    if ( cpu_hardware.size() > 0 )
    {
        cpu_disp_name += " | ";
        cpu_disp_name += cpu_hardware;
    }

    if ( cpu_revision.size() > 0 )
    {
        cpu_disp_name += ", ";
        cpu_disp_name += cpu_revision;
    }
}
#endif

void read_cpu_info()
{
    char dev_map[256] = {0};
    char data[64] = {0};

    read_cpu_info_proc();

    // Get CPU present min and max.
#ifndef __APPLE__
    if ( readf( DEF_CPU_PRESENT, data, 64 ) == true )
    {
        // split with "-".
        string splitsrc = data;
        int    splpos   = splitsrc.find( "-" );
        if ( splpos != string::npos )
        {
            string cmin = splitsrc.substr( 0, splpos );
            string cmax = splitsrc.substr( splpos + 1 );
            cpu_cnt_min = atoi( cmin.c_str() );
            cpu_cnt_max = atoi( cmax.c_str() );
            cpu_cnt_max++;
        }
    }
#else
    const char tmpfn[] = "/private/tmp/cpucnt";
    snprintf( dev_map, 256, "%s > %s", DEF_CPU_PRESENT, tmpfn );
    system( dev_map );
    if ( readf( tmpfn, data, 64 ) == true )
    {
        unlink( tmpfn );
        string splitsrc = data;
        size_t splpos   = splitsrc.find( ":" );
        if ( splpos != string::npos )
        {
            string cpuss = splitsrc.substr( splpos + 1 );
            cpu_cnt_max = atoi( cpuss.c_str() );
            cpu_cnt_min = 0;
        }
    }
#endif
    size_t cpu_cnts = cpu_cnt_max - cpu_cnt_min;

    if ( cpu_cnts > 0 )
    {
        cpu_freq_max.resize( cpu_cnts );
        cpu_freq_min.resize( cpu_cnts );
        cpu_freq_cur.resize( cpu_cnts );
    }

    for( unsigned cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
    {
        sprintf( dev_map, DEF_CPU_MAX_CLK_MAP, cnt );
    
        if ( readf( dev_map, data, 64 ) == true )
        {
            cpu_freq_max[cnt-cpu_cnt_min] = (float)atoi(data);
        }
        
        sprintf( dev_map, DEF_CPU_MIN_CLK_MAP, cnt );
    
        if ( readf( dev_map, data, 64 ) == true )
        {
            cpu_freq_min[cnt-cpu_cnt_min] = (float)atoi(data);
        }
    }
}

void update_cpu_freq()
{
    char dev_map[256] = {0};

    for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
    {
        sprintf( dev_map, DEF_CPU_CUR_CLK_MAP, (unsigned)cnt );
    
        char data[64] = {0};

        if ( readf( dev_map, data, 64 ) == true )
        {
            cpu_freq_cur[cnt-cpu_cnt_min] = (float)atoi(data);
        }
    }
}

void applycols( Fl_Output* d, Fl_Double_Window* s )
{
    d->color( fl_darker( s->color() ) );
    d->labelcolor( s->labelcolor() );
    d->labelsize( s->labelsize() );

    d->textcolor( 0xFF663300 );
    d->textfont( FL_COURIER );
    d->textsize( s->labelsize() );
}

Fl_RGB_Image* loadIcon( Fl_Double_Window* w )
{
    if ( w == NULL )
        return NULL;

    string trypath = "/usr/local/share/icons/hicolor/256x256/apps";
    string fpath = trypath + "/rkcpumon.png";
    bool   existed = false;

    if ( access( fpath.c_str() , 0 ) == 0 )
    {
        existed = true;
    }
    else
    {
        trypath = getenv( "PWD" );
        fpath = trypath + "/res/rkcpumon.png";

        if ( access( fpath.c_str(), 0 ) == 0 )
        {
            existed = true;
        }
    }

    if ( existed == true )
    {
        Fl_RGB_Image* imgIcon = new Fl_PNG_Image( fpath.c_str() );
        if ( imgIcon != NULL )
        {
#ifndef __APPLE__
            w->icon( imgIcon );
#endif
            return imgIcon;
        }
    }

    return NULL;
}

void generateComponents()
{
    unsigned colBg = 0x333333FF;
    unsigned colFg = 0xAAAAAAFF;
    unsigned lblSz = 12;

    unsigned ww = 400;
    unsigned wh = 520;

#ifdef APPTITLE
    strAppTitle = APPTITLE;
#else
    strAppTitle  = DEF_APP_NAME;
#endif
    strAppTitle += " version ";
    strAppTitle += DEF_APP_VERSION;

    winMain = new Fl_Double_Window( ww, wh, strAppTitle.c_str() );
    if ( winMain != NULL )
    {
        winIcon = loadIcon( winMain );
        winMain->color( colBg );
        winMain->labelcolor( colFg );
        winMain->labelsize( lblSz );

        int putx = 100;
        int puty = 10;
        int putw = winMain->w() - putx - 5;
        int puth = 20;

        outCPUname = new Fl_Output( putx, puty, putw, puth,
                                    "CPU name:" );
        if ( outCPUname != NULL )
        {
            applycols( outCPUname, winMain );
            puty += puth + 5;
        }

        outCPUfeatures = new Fl_Output( putx, puty, putw, puth,
                                      "CPU features:" );
        if ( outCPUfeatures != NULL )
        {
            applycols( outCPUfeatures, winMain );
            puty += puth + 5;
        }

        putx = 5;
        putw = winMain->w() - 10;
        puth = 100;

        boxGraphs = new Fl_Box( putx, puty, putw, puth );
        if ( boxGraphs != NULL )
        {
            arraylen = ( putw / arraydiv ) + 2;

            // generate buffers stored each temperatures.
            arrayCPUt.resize( arraylen );
            arrayGPUt.resize( arraylen );
  
            imgGraphs = fl_imgtk::makeanempty( boxGraphs->w(),
                                               boxGraphs->h(),
                                               4,
                                               0x000000FF );
            boxGraphs->align( FL_ALIGN_INSIDE | FL_ALIGN_TOP_LEFT 
                              | FL_ALIGN_IMAGE_BACKDROP );
 
            boxGraphs->box( FL_FLAT_BOX );
            boxGraphs->color( 0 );
            boxGraphs->image( imgGraphs );

            Fl_Box* boxInfoCPU = new Fl_Box( putx+2, puty+2, putw/2, 10,
                                             "--- CPU" );
            if ( boxInfoCPU != NULL )
            {
                boxInfoCPU->align( FL_ALIGN_INSIDE | FL_ALIGN_TOP_LEFT );
                boxInfoCPU->labelcolor( DEF_DRAW_LINE_COL_C );
                boxInfoCPU->labelfont( FL_COURIER );
                boxInfoCPU->labelsize( 8 );    
            }

            Fl_Box* boxInfoGPU = new Fl_Box( putx+2, puty+12, putw/2, 10,
                                             "--- GPU" );
            if ( boxInfoGPU != NULL )
            {
                boxInfoGPU->align( FL_ALIGN_INSIDE | FL_ALIGN_TOP_LEFT );
                boxInfoGPU->labelcolor( DEF_DRAW_LINE_COL_G );
                boxInfoGPU->labelfont( FL_COURIER );
                boxInfoGPU->labelsize( 8 );    
            }

            puty += puth + 5;
        }

        putx = 100;
        puth = 15;
        putw = winMain->w() - 110;

        for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
        {
            MulticolorBarGraph* mbgOne =\
                new MulticolorBarGraph( putx, puty, putw, puth );
            if ( mbgOne != NULL )
            {
                mbgOne->color( fl_darker( winMain->color() ) );
                mbgOne->AddBar(0.0, 0x72CC6B00 );
                mbgOne->AddBar(0.0, 0x10579300 );

                char tmpstr[40] = {0};
                sprintf( tmpstr, "CPU clk. #%lu :", cnt );
                mbgOne->labelcolor( winMain->labelcolor() );
                mbgOne->labelsize( winMain->labelsize() - 2 );
                mbgOne->copy_label( tmpstr );
                mbgOne->align( FL_ALIGN_LEFT );

                mbgCPUfreq.push_back( mbgOne );

                puty += puth + 2;
            }
        }

        putw = ( winMain->w() / 3 ) - 5;
        puth = 20;

        char tmpstr[128] = {0};

        for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
        {
            int tmpx = putx;
			size_t idx = cnt - cpu_cnt_min;

            sprintf( tmpstr, "#%lu clk range:", cnt );

            Fl_Output* o1 = new Fl_Output( putx, puty, putw, puth,
                                           strdup( tmpstr ) );
            tmpx += ( putw * 1.5f );
            snprintf( tmpstr, 128, "clk cur:" );

            Fl_Output* o2 = new Fl_Output( tmpx, puty, putw / 2, puth,
                                           strdup( tmpstr ) );

            tmpx += putw/2;
            int tmpw = ww - tmpx - 10;

            Fl_Box* b1 = new Fl_Box( tmpx, puty, tmpw, puth, "MHz" );
            if ( b1 != NULL )
            {
                b1->box( FL_FLAT_BOX );
                b1->color( winMain->color() );
                b1->labelcolor( winMain->labelcolor() );
                b1->labelsize( winMain->labelsize() );
                b1->labelfont( winMain->labelfont() );
            }
 
            applycols( o1, winMain );
            applycols( o2, winMain );

            outCPUfreqMinMax.push_back( o1 );
            outCPUfreqCur.push_back( o2 );
            boxMHzdp.push_back( b1 );

            puty += puth + 5;
        }

        putx = 150;
        putw = winMain->w() - putx - 5;
        puth = 20;

        outCPUtemper = new Fl_Output( putx, puty, putw, puth,
                                      "CPU temperature:" );
        if ( outCPUtemper != NULL )
        {
            applycols( outCPUtemper, winMain );
            puty += puth + 5;
        }

        outGPUtemper = new Fl_Output( putx, puty, putw, puth,
                                      "GPU temperature:" );
        if ( outGPUtemper != NULL )
        {
            applycols( outGPUtemper, winMain );
            puty += puth + 5;
        }

        // auto resize window ...
        int wx = winMain->x();
        int wy = winMain->y();
        wh = puty + 35;
        winMain->resize( wx, wy, ww, wh );

        puth = 30;
        putx = 5;
        putw = winMain->w() - 10;
        puty = winMain->h() - puth;

        boxCopyright = new Fl_Box( putx, puty, putw, puth,
                                   "(C)Copyright 2021..2024 rageworx@@gmail.com" );
        if ( boxCopyright != NULL )
        {
            boxCopyright->align( FL_ALIGN_INSIDE | FL_ALIGN_RIGHT );
            boxCopyright->labelfont( winMain->labelfont() );
            boxCopyright->labelsize( winMain->labelsize() );
            boxCopyright->labelcolor( winMain->labelcolor() );
        }

        winMain->end();
        winMain->show();
    }
}

void updateGraphs()
{
    for( size_t cnt=1; cnt<arraylen; cnt++ )
    {
        arrayCPUt[cnt-1] = arrayCPUt[cnt];
        arrayGPUt[cnt-1] = arrayGPUt[cnt];
    }

    arrayCPUt[ arraylen - 1 ] = cpu_temper;
    arrayGPUt[ arraylen - 1 ] = gpu_temper;

    float tf = (float)temp_thres / (float)temp_max;
    float tv = (float)imgGraphs->h() - ( (float)imgGraphs->h() * tf );

    static int shifts = DEF_SHIFT_PIXELS;

    // draw 
    for( int x=0; x<imgGraphs->w(); x++ )
    {
        float    tv1 = tv;
        float    tv2 = tv * 2;
        unsigned col = DEF_SHIFT_PIXELS;
        
        if ( ( x - shifts ) >= 0 )
           if ( ( x - shifts ) % 30 == 0 )
                col = DEF_DRAW_BG_COL_0;

        fl_imgtk::draw_line( imgGraphs,
                             x, 0, x, tv1, col );
        
        col = DEF_DRAW_BG_COL_2;
        
        if ( ( x - shifts ) >= 0 )
           if ( ( x - shifts ) % 30 == 0 )
                col = DEF_DRAW_BG_COL_1;

        fl_imgtk::draw_line( imgGraphs,
                             x, tv1+1, x, tv2, col );


        col = DEF_DRAW_BG_COL_3;

        if ( ( x - shifts ) >= 0 ) 
            if ( ( x - shifts ) % 30 == 0 )
                col = DEF_DRAW_BG_COL_4;

        fl_imgtk::draw_line( imgGraphs,
                             x, tv2+1, x, imgGraphs->h(), col );
    }

    shifts += DEF_SHIFT_MIGR;

    if ( shifts <= 0 )
    {
        shifts = DEF_SHIFT_PIXELS;
    }

    const char lnthk = DEF_DRAW_LINE_THCK;

    tf = (float)imgGraphs->h() / (float)temp_max;

    for( size_t cnt=1; cnt<arraylen; cnt++ )
    {
        float tvf1   = arrayGPUt[cnt-1] * tf;
        unsigned py1 = imgGraphs->h() - (unsigned)tvf1;
        float tvf2   = arrayGPUt[cnt] * tf;
        unsigned py2 = imgGraphs->h() - (unsigned)tvf2;
        unsigned px1 = (cnt-1)*arraydiv;
        unsigned px2 = (cnt*arraydiv)-(lnthk/2.f+0.5f);

        if ( py1 > imgGraphs->h() )
            py1 = imgGraphs->h() - (lnthk/2.f);

        if ( py2 > imgGraphs->h() )
            py2 = imgGraphs->h() - (lnthk/2.f);

        const ulong colGPU = DEF_DRAW_LINE_COL_G;
        const ulong colCPU = DEF_DRAW_LINE_COL_C;

        if ( gpu_tmp_idx >= 0 )
        {
            fl_imgtk::draw_smooth_line_ex( imgGraphs,
                                           px1, py1,
                                           px2, py2,
                                           lnthk,
                                           colGPU );
        }

        if ( cpu_tmp_idx >= 0 )
        {
            tvf1 = arrayCPUt[cnt-1] * tf;
            py1 = imgGraphs->h() - (unsigned)tvf1;
            tvf2 = arrayCPUt[cnt] * tf;
            py2 = imgGraphs->h() - (unsigned)tvf2;

            fl_imgtk::draw_smooth_line_ex( imgGraphs,
                                           px1, py1,
                                           px2, py2,
                                           lnthk,
                                           colCPU );
        }
    }

    imgGraphs->uncache();
}

void fillPreInfo()
{
    outCPUname->value( cpu_disp_name.c_str() );
    outCPUfeatures->value( cpu_disp_features.c_str() );

    for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
    {
        char tmpstr[64] = {0};
        size_t idx = cnt - cpu_cnt_min;

        sprintf( tmpstr, "%.1f ~ %.1f",
                 cpu_freq_min[idx] / 1000.f,
                 cpu_freq_max[idx] / 1000.f );

        outCPUfreqMinMax[idx]->value( strdup( tmpstr ) );

        if ( cpu_freq_maxall < cpu_freq_max[idx] )
        {
            cpu_freq_maxall = cpu_freq_max[idx];
        }
    }

    for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
    {
        size_t idx = cnt - cpu_cnt_min;

        mbgCPUfreq[idx]->SetBarValue(1, 
                                     cpu_freq_max[idx] / cpu_freq_maxall );
    }
}

void fillCPUinfo()
{
    static char cpucurfreq[64][64] = {0};
    static char cputemper[64] = {0};
    static char gputemper[64] = {0};

    for( size_t cnt=cpu_cnt_min; cnt<cpu_cnt_max; cnt++ )
    {
        size_t idx = cnt - cpu_cnt_min;
        sprintf( cpucurfreq[idx],
                 "%.1f",
                 cpu_freq_cur[idx] / 1000.f );

        outCPUfreqCur[idx]->value( cpucurfreq[idx] );
        mbgCPUfreq[idx]->SetBarValue(0, 
                                     cpu_freq_cur[idx] / cpu_freq_maxall );
    }
 
    cpu_temper = read_temperature( 0 );
    gpu_temper = read_temperature( 1 );

    if ( cpu_temper >= -90.f )
    {
        snprintf( cputemper, 64,
                  "%4.1f %-4.*s",
                  cpu_temper,
                  INT_STR_PAIR(4, "℃") );
    }
    else
    {
        sprintf( cputemper, "N/A" );
    }

	if ( gpu_temper >= -90.f )
    {
		snprintf( gputemper, 64,
                  "%4.1f %-4.*s", 
                  gpu_temper,
                  INT_STR_PAIR(4, "℃") );
	}
	else
    {
        sprintf( gputemper, "N/A" );
    }

    outCPUtemper->value( cputemper );
    outGPUtemper->value( gputemper );
}

// 
static bool sighandling = false;

void sighandler( int sig )
{
    if ( sighandling == false )
    {
        sighandling = true;

        printf( "\nSIGNAL:%d\n",sig );

        threadlived = false;
        pthread_cancel( pt );
        pthread_join( pt, 0 );
        exit( 0 );
    }
}

void* threadRun( void* p )
{
    while( threadlived == true )
    {
        update_cpu_freq();
        Fl::lock();
        fillCPUinfo();
        boxGraphs->image(NULL);
        updateGraphs();
        if( imgGraphs != NULL ) /// possible case ???
        {
            boxGraphs->image(imgGraphs);
            boxGraphs->redraw();
        }
        Fl::unlock();
        winMain->redraw();
        Fl::awake();

        usleep( 1000000 );
    }

    pthread_exit( 0 );
    return NULL;
}

int main( int argc, char** argv )
{
    signal( SIGINT, sighandler );

    Fl_Window::default_xclass( DEF_APP_CLS );
    Fl::scheme( "flat" );
    Fl::lock();

    // find some fonts for right print out.
    // Chnage FL_COURIER to "BFreeMono" if existed.
    size_t fntlen = 0;

#if defined(FLTK_USE_X11) && !defined(FLTK_USE_CAIRO)
    // ask for everything that claims to be iso10646 compatible
    fntlen = Fl::set_fonts("-*-*-*-*-*-*-*-*-*-*-*-*-iso10646-1");
#else
    // ask for everything
    fntlen = Fl::set_fonts("*");
#endif

    for( size_t cnt=FL_FREE_FONT; cnt<fntlen; cnt++ )
    {
        const char* fntnm = Fl::get_font( (Fl_Font)cnt );
        if ( fntnm != NULL )
        {
            if ( strcmp( fntnm, "BFreeMono" ) == 0 )
            {
                Fl::set_font( FL_COURIER, (Fl_Font)cnt );
                break;
            }
        }
    }

    sensetmpdevs();
    read_cpu_info();
    generateComponents();
    fillPreInfo();

    pthread_create( &pt, NULL, threadRun, NULL );

    int reti = Fl::run();

    threadlived = false;
    pthread_cancel( pt );
    pthread_join( pt, 0 );

    fl_imgtk::discard_user_rgb_image( winIcon );

    return reti;
}
