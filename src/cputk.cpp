#include <unistd.h>
#include <dirent.h>
#include <cstring>
#include "cputk.h"

#define DEF_MAP_SYS_DEVICES     "/sys/devices"

using namespace std;

unsigned serach_cpu_dirs( vector< string > &list )
{
    DIR* dir = opendir( DEF_MAP_SYS_DEVICES );
    
    if ( dir != NULL )
    {
        list.clear();

        struct dirent* dent = readdir( dir );

        while( dent != NULL )
        {
            // check name length bigger than 2.
            if ( strlen( dent->d_name ) > 2 )
            {
                string cfn = dent->d_name;

                // find arm* name scheme.
                if ( cfn.find( "arm" ) != string::npos )
                {
                    list.push_back( cfn );
                }
            }
            dent = readdir( dir );
        }

        closedir( dir );
    }

    return list.size();
}
